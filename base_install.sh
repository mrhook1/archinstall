#!/bin/bash
source ./config.sh

## setting up base info
echo "Setting timezone and hwclock";
ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime
hwclock --systohc
timedatectl set-ntp true
sleep 3
echo "Setting locale"
sed -i '177s/#//g' /etc/locale.gen
sed -i '444s/#//g' /etc/locale.gen
locale-gen
export LANG=en_US.UTF-8
echo "LANG=$b_lang1" >> /etc/locale.conf
echo "KEYMAP=$b_keymap" >> /etc/vconsole.conf
sleep 3
echo "Setting hostname and hosts"
echo "$b_hostname" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $b_hostname.localdomain $b_hostname" >> /etc/hosts
sleep 3
## enable multilib
echo "Editing pacman.conf"
sed -i '93s/#//g' /etc/pacman.conf
sed -i '94s/#//g' /etc/pacman.conf

## pacman colors
sed -i '33s/#//g' /etc/pacman.conf
sleep 3
## installing needed apps
echo "Installing kernel, needed applications"
pacman -Sy -y --noconfirm
pacman -S -y --noconfirm reflector rsync xdg-user-dirs
reflector -c $b_country -a 12 --sort rate --save /etc/pacman.d/mirrorlist
pacman -S -y --noconfirm linux linux-headers linux-firmware sof-firmware grub efibootmgr networkmanager dialog wpa_supplicant os-prober vim archey3 dhcpcd iw iwd netctl


sleep 3
## user setup
echo "Adding root password and a new user with Sudo priv."
echo root:$b_password | chpasswd
useradd -m -g users -G wheel,storage,power -s /bin/bash $b_username
echo $b_username:$b_password | chpasswd
sed -i '85s/#//g' /etc/sudoers

sleep 3
## bash colors 
echo "Making bash colorful"
xdg-user-dirs-update
cp bash.bashrc /etc
cp DIR_COLORS /etc
#cp bash.bashrc /home/$b_username/.bashrc
rm -rf /home/$b_username/.bashrc

## uncheck your gpu
# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings
# pacman -S virtualbox-guest-utils

## uncheck your cpu
# intel_ucode
# amd_ucode

sleep 3
## uefi
echo " Installing Grub and enabling service for Network Manager"
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

## network
#systemctl enable NetworkManager
