#!/bin/bash

source ./config.sh

echo "Installing packages"
sudo pacman -S -y --noconfirm xorg-server xorg-xinit xfce4 xfce4-goodies network-manager-applet mtools dosfstools avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups alsa-utils pipewire pipewire-pulse pipewire-alsa pipewire-jack bash-completion openssh acpi acpi_call tlp openbsd-netcat firewalld flatpak sof-firmware nss-mdns acpid ntfs-3g terminus-font lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings firefox vlc libreoffice-fresh-sv leafpad htop

sleep 5

echo "Enabling services:"
sudo systemctl enable bluetooth
sudo systemctl enable cups.service
sudo systemctl enable sshd
sudo systemctl enable avahi-daemon
sudo systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
sudo systemctl enable reflector.timer
sudo systemctl enable fstrim.timer
#sudo systemctl enable libvirtd
sudo systemctl enable firewalld
sudo systemctl enable acpid

sleep 5
echo "Configuring the firewall"
sudo systemctl enable firewalld
sudo systemctl start firewalld
sleep 3
sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload



sleep 5
echo "Enabling service lightdm"
localectl set-keymap sv-latin1
sudo systemctl enable lightdm
