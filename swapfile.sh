#!/bim/bash
echo "Must be loged in as root"
#sudo su
## adding swapfile
dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile none swap defaults 0 0" >> /etc/fstab

echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf
